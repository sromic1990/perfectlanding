﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ropeTest : MonoBehaviour
{
    public List<HingeJoint> joints;
    public List<Rigidbody> jointsRb;
    [FormerlySerializedAs("player")] public Rigidbody rope,player;

    public List<HingeJoint> tempJoints;

    private List<Rigidbody> tempJointsRb;
    private GameObject grappingLine;
    private Vector3 camPos;
    [SerializeField]private int count = 0,tempCount;
    public bool isPressed = false;
    // Start is called before the first frame update
    void Start()
    {
        camPos = Camera.main.transform.position;
        tempJoints = joints;
        tempJointsRb = jointsRb;
        grappingLine=new GameObject("GrappingLine");
        grappingLine.SetActive(false);
        LineRenderer line_renderer = grappingLine.AddComponent<LineRenderer>();

        line_renderer.endWidth = .1f;
        line_renderer.startWidth = .01f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && tempJointsRb.Count>0)
        {
          //  tempJoints[0].connectedBody = player;
          rope.GetComponent<HingeJoint>().connectedBody = tempJointsRb[0];
          rope.transform.position = Vector3.one;
          Vector3 newpos= new Vector3(camPos.x,camPos.y,(camPos.z+tempJointsRb[0].transform.position.z));
          StartCoroutine(MoveCo(Camera.main.transform, Camera.main.transform.position, newpos));
          // Camera.main.transform.position = newpos;
        }

        if (Input.GetMouseButtonDown(0) && tempJointsRb.Count <= 0)
        {
            player.GetComponent<HingeJoint>().connectedBody = null;
        }

        if (Input.GetMouseButtonUp(0) && tempJointsRb.Count>0)
        {
           // tempJoints.RemoveAt(0);
          
           tempJointsRb.RemoveAt(0);
        }
    }

/*
 void Update()
    {
        if (Input.GetMouseButtonDown(0) && tempJoints.Count<joints.Count)
        {
            Debug.Log("pressed");
            player.constraints = RigidbodyConstraints.FreezeRotation;
           // player.freezeRotation = false;
            player.GetComponent<Collider>().isTrigger = true;
            var pos = player.transform.position;
        //   player.transform.position=new Vector3(pos.x,pos.y+1f,pos.z-2f);
            grappingLine.SetActive(true);
            tempJoints.Add(joints[count]);
          
          //  player.GetComponent<Animator>().Play("Swing");
          //  player.GetComponent<CharacterAnimation>().animator.Play( CharacterAnimStates.Swing.ToString());
        
          var motor = tempJoints[count].motor;
          motor.force = 100;
          motor.targetVelocity = 90;
        //  tempJoints[count].motor = motor;
        //  tempJoints[count].useMotor = true;
        var hinge = tempJoints[count];
        var diff = Vector3.Distance(hinge.transform.position, player.transform.GetChild(0).position);
        Debug.Log("anchor y=="+(hinge.transform.position.y-player.transform.GetChild(0).localPosition.y));
        rope.GetComponent<HingeJoint>().connectedBody = jointsRb[count];
//        tempJoints[count].connectedAnchor=new Vector3(hinge.connectedAnchor.x,diff,hinge.connectedAnchor.z);
//        tempJoints[count].connectedBody = player;
        tempCount = count;
          //  player.transform.position = Vector3.one;
            Vector3 newpos= new Vector3(camPos.x,camPos.y,(camPos.z+tempJoints[count].transform.position.z));
       //  StartCoroutine(MoveCo(camera.transform, camera.transform.position, newpos));
           count += 1;
           isPressed = true;
        }

        if (tempJoints.Count > 0 && isPressed)
        {
            Vector3[] line_vertex_arr = {player.transform.GetChild(0).position, tempJoints[tempCount].transform.position};
            grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
          //  speedText.text = ""+tempJoints[tempCount].velocity;
            
        }

        if (Input.GetMouseButtonUp(0) )//&& tempJoints.Count==joints.Count)
        {
            isPressed = false;
            rope.GetComponent<HingeJoint>().connectedBody = null;
         //   player.GetComponent<Animator>().Play(CharacterAnimStates.Landing.ToString());
            for (int i = 0; i < joints.Count; i++)
            {
                 joints[i].connectedBody = null;
            }
            player.GetComponent<Collider>().isTrigger = false;
          player.rotation=Quaternion.Euler(0,0,0);
          player.constraints = RigidbodyConstraints.FreezeRotation;
           // player.freezeRotation = true;
            grappingLine.SetActive(false);
//            var camDiff = pillers[tempCount].transform.localPosition.z - camera.transform.position.z;
//          //  if (tempCount == 0)
//                camDiff = camDiff * 2f;
//            Debug.Log(camera.transform.position.z+"z>>"+camDiff);
//            Vector3 newpos= new Vector3(camPos.x,camPos.y,(camera.transform.position.z+camDiff));
//            Debug.Log("newpos"+newpos);
           // StartCoroutine(MoveCo(camera.transform.position, newpos));
           // tempJoints.RemoveAt(0);
        }
    }
 */
    IEnumerator MoveCo(Transform _transform,Vector3 _from,Vector3 _to)
    {
        float t = 0f,speed=2f;
        Debug.Log("dist=="+Vector3.Distance(_to, _from));
        while (t<1)
        {
            t += Time.deltaTime*speed;
            _transform.position = Vector3.Lerp(_from, _to, t);
            Camera.main.transform.position = _transform.position;
            yield return null;
        }
      
    }
}
