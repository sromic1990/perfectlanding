﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sourav.Engine.Core.GameElementRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _PerfectLanding.Scripts.View.Animation
{
    public enum CharacterAnimStates
    {
        Empty,
        Walk,
        ThrowRope,
        JumpFromPlatfrom,
        JumpPlatformSame,
        Swing,
        Landing,
        LandingSamePlace
    }
    public class CharacterAnimation : GameElement
    {
        public CharacterAnimStates animStates;

        public Animator animator;
        public Transform swingPivotTransform;
        public bool isMoving;
        // Start is called before the first frame update
        void Start()
        {
            animator = this.GetComponent<Animator>();
        }

   
        public void OnMovementStart()
        {
            App.GetNotificationCenter().Notify(Notification.StartCameraScript);
        }

        public void OnMovementComplete()
        {
            App.GetNotificationCenter().Notify(Notification.StopCameraScript);
        }

        IEnumerator OnCompleteAnimation()
        {
            while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime<1.0f)
            {
                yield return null;
            }

            if (animator.GetCurrentAnimatorStateInfo(0).IsName(CharacterAnimStates.Landing.ToString()))
            {
                //notify
                App.GetNotificationCenter().Notify(Notification.Landed);
            }

        }

    }

}

