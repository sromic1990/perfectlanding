﻿using Sourav.Engine.Core.GameElementRelated;
using UnityEngine;
using UnityEngine.UI;

namespace _PerfectLanding._Scripts.View.Elements
{
    public class ScoreView : GameElement
    {
        [SerializeField] private Text scoreText;
        [SerializeField] private Text bestScoreText;


        public void SetUpCurrentScore(int score)
        {
            scoreText.text = score.ToString(); //App.GetLevelData().CurrentScore.ToString();
        }


        public void SetHighestScore(int highestScore)
        {
            bestScoreText.text = highestScore.ToString();
        }
    }
}
