﻿using System;
using Sourav.Engine.Core.GameElementRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _PerfectLanding._Scripts.View.Elements
{
    public class GroundElement : GameElement
    {
        private void OnCollisionEnter(Collision other)
        {
            App.GetNotificationCenter().Notify(Notification.GameOver);
        }
    }
}
