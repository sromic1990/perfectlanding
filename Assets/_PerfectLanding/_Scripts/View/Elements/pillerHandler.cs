﻿using System.Collections;
using _PerfectLanding.Scripts.View.Animation;
using Sourav.Engine.Core.GameElementRelated;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _PerfectLanding._Scripts.View.Elements
{
    public class pillerHandler : GameElement
    {
        public int index;
        public Transform jumpingPosition;
        
        public Collider centerArea,yellowArea;
        // Start is called before the first frame update
        void Start()
        {
        
        }
        void OnCollisionEnter(Collision collision)
        {
            Vector3 normal = collision.contacts[0].normal;
   
       
       
            if(normal == transform.forward)
            {
                Debug.Log("WORKED FORWARD");
           
            }
       
            else if(normal == -(transform.forward))
            {
                Debug.Log(index+"WORKED BACKWARD"+App.GetCharacterData().count);
               
//                App.GetCharacterData().characterAnimation.animator.Play(CharacterAnimStates.Empty.ToString());
                collision.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                if (App.GetCharacterData().count > 0 && App.GetCharacterData().count==index)
                {
                    if (centerArea.bounds.Contains(collision.transform.position))
                    {
                        Debug.Log("within center");
                        App.GetLevelData().CurrentScore += App.GetCharacterData().redAreaScore;
                        App.GetNotificationCenter().Notify(Notification.UpdateScore);
                    }
                    else if (yellowArea.bounds.Contains(collision.transform.position))
                    {
                        Debug.Log("within yellow area");
                        App.GetLevelData().CurrentScore += App.GetCharacterData().YellowAreaScore;
                        App.GetNotificationCenter().Notify(Notification.UpdateScore);
                    }
                    else
                    {
                        App.GetLevelData().CurrentScore += App.GetCharacterData().SideScore;
                        App.GetNotificationCenter().Notify(Notification.UpdateScore);
                    }
                }
                //check for same piller
                if (App.GetCharacterData().count != index)
                {
                    NotificationParam param = new NotificationParam(Mode.intData);
                    param.intData.Add(index);
                    App.GetNotificationCenter().Notify(Notification.SamePillerArrived,param);
                }

            

                //after landing move to jumping position
                StartCoroutine(MoveCo(collision.gameObject, collision.transform.GetComponent<Animator>(), collision.transform.position,
                    jumpingPosition.position));
            }
       
            else if(normal == transform.right)
            {
                Debug.Log("WORKED RIGHT");
           
            }
       
            else if(normal == -(transform.forward))
            {
                Debug.Log("WORKED LEFT");
           
            }
       
            else if(normal == transform.up)
            {
                Debug.Log("CIRCLE");
           
            }
       
            else if(normal == -(transform.up))
            {
                Debug.Log("WORKED DOWN");
           
            }
            else
            {
           
                return;
            }
        }
//    private void OnCollisionEnter(Collision other)
//    {
//        Debug.Log("collision "+other.gameObject.name);
//        other.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
//        
//        //after landing move to jumping position
//        StartCoroutine(MoveCo(other.gameObject, other.transform.GetComponent<Animator>(), other.transform.position,
//            jumpingPosition.position));
//    }

        IEnumerator MoveCo(GameObject g, Animator animator,Vector3 _from, Vector3 _to)
        {
            while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime<1.0f)
            {
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);
            Debug.Log(g.transform.position+"is destination"+Vector3.Distance(g.transform.position,_to));
            float t = 0f, speed = 0.5f;
            Vector3 toPos=new Vector3(g.transform.position.x,g.transform.position.y,_to.z);
            animator.Play(CharacterAnimStates.Walk.ToString());
//            while (Mathf.Abs(Vector3.Distance(g.transform.position,_to))>0.1f)
            if (_to.z > g.transform.position.z)
            {
                while (Vector3.Distance(_to,g.transform.position)>0.1f)
                {
                    t += Time.deltaTime * speed;
                    g.transform.position = Vector3.Lerp( _from, toPos,t);
                    yield return null;
                }
            }
           

            g.transform.position = toPos;
//        animator.Play(CharacterAnimStates.ThrowRope.ToString());
//        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime<1.0f)
//        {
//            yield return null;
//        }
//            animator.Play(CharacterAnimStates.Swing.ToString());

            App.GetNotificationCenter().Notify(Notification.Reached);
//            grapHook.Instance.CreateRope();
        }
    }
}
