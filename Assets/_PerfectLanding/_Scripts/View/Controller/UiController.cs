﻿using System.Collections.Generic;
using _PerfectLanding._Scripts.View.Elements;
using MEC;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using Sourav.Engine.Editable.PopUpRelated.Element;
using Sourav.Engine.Editable.PopUpRelated.Model;
using UnityEditor;
using UnityEngine;

namespace _PerfectLanding._Scripts.View.Controller
{
    public class UiController : Sourav.Engine.Core.ControllerRelated.Controller
    {
        [Sirenix.OdinInspector.Title("Views")] [SerializeField]
        private ScoreView scoreView;

        [SerializeField] private PopUpView popUpView;
        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {

            switch (notification)
            {
                case Notification.InputDetected:
                    if (popUpView.IsPopUpOen()) return;
                    App.GetNotificationCenter().Notify(Notification.Input);
                    break;
                case Notification.UpdateScore:
                    scoreView.SetUpCurrentScore(App.GetLevelData().CurrentScore);
                    break;
                case Notification.SetHighestScore:
                    scoreView.SetHighestScore(App.GetLevelData().HighScore);
                    break;
                case Notification.GameOver:
                    Timing.RunCoroutine(WaitAndShowGameOver());
                    break;
                case Notification.LevelComplete:
                    Debug.Log("Level complete");
                    popUpView.ShowPopUp(PopUpType.LevelComplete);
                    break;
                case Notification.ShowPopUp:
                    popUpView.ShowPopUp((PopUpType)param.intData[0]);
                    break;
                case Notification.ClosePopUp:
                    popUpView.HidePopUp();
                    break;
            }
        }
        
        
        private IEnumerator<float> WaitAndShowGameOver()
        {
            yield return Timing.WaitForSeconds(App.GetTimerData().playerDieDelay);
//            popUpView.HidePopUpSilently();
            popUpView.ShowPopUp(PopUpType.GameOver);
        }
    }
}
