﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabblingHook : MonoBehaviour
{

    public Rigidbody rigibody;

    public GameObject hooked_node;

    private float node_distance;

    private float start_node_distance;

    private GameObject grappingLine;
    
    // Start is called before the first frame update
    void Start()
    {
        grappingLine=new GameObject("GrappingLine");
        grappingLine.SetActive(false);
        LineRenderer line_renderer = grappingLine.AddComponent<LineRenderer>();
        line_renderer.endWidth = .1f;
        line_renderer.startWidth = .01f;
        rigibody.mass = 0.2f;
        // rigibody.useGravity = false;
        // hooked_node = null;
    }

    // Update is called once per frame
    void Update()
    {
       // if (Input.GetMouseButtonDown(0))
       // {
            node_distance = start_node_distance =
                Vector3.Distance(hooked_node.transform.position, gameObject.transform.position);
            grappingLine.SetActive(true);
        //}
    }

    private void FixedUpdate()
    {
        if (hooked_node != null && Input.GetMouseButton(0))
        {
            Vector3[] line_vertex_arr = {gameObject.transform.position, hooked_node.transform.position};
            grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
            //gets velocity in units/frame, then gets the position for next frame
            Vector3 curr_velo_upf = rigibody.velocity * Time.fixedDeltaTime;
            Vector3 test_pos = gameObject.transform.position + curr_velo_upf;
            
            ApplyTensionForce(curr_velo_upf, test_pos);
        }

        if (Input.GetMouseButtonUp(0))
        {
            rigibody.AddForce(rigibody.velocity,ForceMode.Impulse);
            //ApplyTensionForce(curr_velo_upf, test_pos);
            grappingLine.SetActive(false);

        }
    }
    
    private void ApplyTensionForce(Vector3 curr_velo_upf, Vector3 test_pos)
    {
        //finds what the new velocity is due to tension force grappling hook
        //normalized vector that from node to test pos
        Vector3 node_to_test = (test_pos - hooked_node.transform.position).normalized;
        Vector3 new_pos = (node_to_test * node_distance) + hooked_node.transform.position;
        Vector3 new_velocity = new_pos - gameObject.transform.position;

        //force_tension = mass * (d_velo / d_time)
        //where d_velo is new_velocity - old_velocity
        Vector3 delta_velocity = new_velocity - curr_velo_upf;
        //Vector3 tension_force = (1f * (delta_velocity / Time.fixedDeltaTime));
        Vector3 tension_force = (rigibody.mass * (delta_velocity / Time.fixedDeltaTime));

        rigibody.AddForce(tension_force, ForceMode.Impulse);
    }
}
