﻿using System;
using System.Collections;
using System.Collections.Generic;
using _PerfectLanding.Scripts;
using _PerfectLanding.Scripts.View.Animation;
using DG.Tweening;
using MEC;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class grapHook : MonoBehaviour
{
    public List<GameObject> pillers;
    public List<HingeJoint> joints;
    public List<HingeJoint> tempJoints;
    public Camera camera;
    public Rigidbody player;
    private Vector3 camPos;
    
    private GameObject grappingLine;
    public Text speedText;
    [SerializeField]private int count = 0,tempCount;
    public bool isPressed = false;
    public AnimationCurve curve;
    private static grapHook instance;

    public static grapHook Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<grapHook>();
                if (instance == null)
                {
                    GameObject container=new GameObject("GrapHook");
                    instance = container.AddComponent<grapHook>();
                }
                
            }

            return instance;
        }
    
    }

    
    // Start is called before the first frame update
    void Start()
    {
        tempJoints=new List<HingeJoint>();
        grappingLine=new GameObject("GrappingLine");
        grappingLine.SetActive(false);
        camPos = camera.transform.position;
        for (int i = 0; i < joints.Count; i++)
        {
          //  tempJoints.Add(joints[i]);
        }
      //  LineRenderer line_renderer = player.gameObject.AddComponent<LineRenderer>();
        LineRenderer line_renderer = grappingLine.AddComponent<LineRenderer>();

        line_renderer.endWidth = .1f;
        line_renderer.startWidth = .01f;
    }


    public void CreateRope()
    {
        if (joints.Count == count)
            return;
        Vector3[] line_vertex_arr = {player.transform.GetChild(0).position, joints[count].transform.position};
        Debug.Log("rope"+Vector3.Distance(joints[count].transform.position,player.transform.GetChild(0).position));
        Debug.Log("len"+(joints[count].transform.position.y-player.transform.GetChild(0).localPosition.y));
        grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
        grappingLine.SetActive(true);
       // player.GetComponent<CharacterAnimation>().OnMovementComplete();
        
    }
/*
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
           
//            this.GetComponent<Pendulum>().ResetPendulumForces();
            isPressed = true;
            player.constraints = RigidbodyConstraints.None;
            player.constraints = RigidbodyConstraints.FreezeRotation;
            tempCount = count;
            count += 1;
        }

        if (Input.GetMouseButton(0))
        {
            isPressed = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
           
            this.GetComponent<Pendulum>().ResetPendulumForces();
        //    isPressed = false;
            player.GetComponent<Animator>().Play(CharacterAnimStates.Landing.ToString());
            grappingLine.SetActive(false);
            player.GetComponent<Collider>().isTrigger = false;
            StartCoroutine(StopForce());
            
            var camDiff = pillers[tempCount].transform.localPosition.z - camera.transform.position.z;
            //  if (tempCount == 0)
            camDiff = camDiff * 2f;
            Debug.Log(camera.transform.position.z+"z>>"+camDiff);
            Vector3 newpos= new Vector3(camPos.x,camPos.y,(camera.transform.position.z+camDiff));
//            Debug.Log("newpos"+newpos);
            StartCoroutine(MoveCo(camera.transform.position, newpos));
        }
    }
*/
    IEnumerator StopForce()
    {
        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(0.1f);
        isPressed = false;
     //   GetComponent<Pendulum>().currentTime = 0f;
        this.GetComponent<Pendulum>().Pivot = joints[count].gameObject;
        this.GetComponent<Pendulum>().ResetData();
//        this.GetComponent<Pendulum>().ResetPendulumForces();
//        this.GetComponent<Pendulum>().PendulumInit();
    }
    

    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && tempJoints.Count<joints.Count)
        {
            player.constraints = RigidbodyConstraints.None;
           // player.freezeRotation = false;
            player.GetComponent<Collider>().isTrigger = true;
            var pos = player.transform.position;
//           player.transform.position=new Vector3(pos.x,pos.y+1f,pos.z-2f);
           
            grappingLine.SetActive(true);
            tempJoints.Add(joints[count]);
            StartCoroutine(moveChar());
          //  player.GetComponent<Animator>().Play("Swing");
            player.GetComponent<CharacterAnimation>().animator.Play( CharacterAnimStates.Swing.ToString());
        /*
          var motor = tempJoints[count].motor;
          motor.force = 100;
          motor.targetVelocity = 90;
        var hinge = tempJoints[count];
        var diff = Vector3.Distance(hinge.transform.position, player.transform.GetChild(0).position);
        Debug.Log("anchor y=="+(hinge.transform.position.y-player.transform.GetChild(0).localPosition.y));
      //  tempJoints[count].connectedAnchor=new Vector3(hinge.connectedAnchor.x,diff,hinge.connectedAnchor.z);
        tempJoints[count].connectedBody = player;
        tempCount = count;
          //  player.transform.position = Vector3.one;
            Vector3 newpos= new Vector3(camPos.x,camPos.y,(camPos.z+tempJoints[count].transform.position.z));
       //  StartCoroutine(MoveCo(camera.transform, camera.transform.position, newpos));
           count += 1;*/
          // isPressed = true;
           
        }

        if (tempJoints.Count > 0 && isPressed)
        {
            Vector3[] line_vertex_arr = {player.transform.GetChild(0).position, tempJoints[tempCount].transform.position};
            grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
            speedText.text = ""+tempJoints[tempCount].velocity;
            
        }

        if (Input.GetMouseButtonUp(0) )//&& tempJoints.Count==joints.Count)
        {
            isPressed = false;
            Timing.CallDelayed(0.01f, () =>
            {
                player.GetComponent<Animator>().Play(CharacterAnimStates.Landing.ToString());
            });
           
            for (int i = 0; i < joints.Count; i++)
            {
                 joints[i].connectedBody = null;
            }
            player.GetComponent<Collider>().isTrigger = false;
          player.rotation=Quaternion.Euler(0,0,0);
          player.constraints = RigidbodyConstraints.FreezeRotation;
           // player.freezeRotation = true;
            grappingLine.SetActive(false);
            var camDiff = pillers[tempCount].transform.localPosition.z - camera.transform.position.z;
          //  if (tempCount == 0)
                camDiff = camDiff * 2f;
            Debug.Log(camera.transform.position.z+"z>>"+camDiff);
            Vector3 newpos= new Vector3(camPos.x,camPos.y,(camera.transform.position.z+camDiff));
//            Debug.Log("newpos"+newpos);
          //  StartCoroutine(MoveCo(camera.transform.position, newpos));
          
        }
    }

    IEnumerator moveChar()
    {
        Vector3 stratPos = player.transform.position;
        var pos = player.transform.position;
        Vector3 endPos=new Vector3(pos.x,pos.y+0.5f,pos.z-1f);
        float t = 0f;
        while (endPos!=player.transform.position)
        {
            t += Time.deltaTime;
            player.transform.position = Vector3.Lerp(stratPos, endPos, curve.Evaluate(t));
            Vector3[] line_vertex_arr = {player.transform.GetChild(0).position, joints[count].transform.position};
            grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
            yield return null;
        }
        player.GetComponent<CharacterAnimation>().OnMovementStart();
        var motor = tempJoints[count].motor;
        motor.force = 100;
        motor.targetVelocity = 90;
        //  tempJoints[count].motor = motor;
        //  tempJoints[count].useMotor = true;
        var hinge = joints[count];
        var diff = Vector3.Distance(hinge.transform.position, player.transform.GetChild(0).position);
        Debug.Log("anchor y=="+(hinge.transform.position.y-player.transform.GetChild(0).localPosition.y));
        //  tempJoints[count].connectedAnchor=new Vector3(hinge.connectedAnchor.x,diff,hinge.connectedAnchor.z);
        tempJoints[count].connectedBody = player;
        tempCount = count;
        //  player.transform.position = Vector3.one;
        Vector3 newpos= new Vector3(camPos.x,camPos.y,(camPos.z+tempJoints[count].transform.position.z));
     //   player.constraints = RigidbodyConstraints.FreezeRotationX;
        //  StartCoroutine(MoveCo(camera.transform, camera.transform.position, newpos));
        count += 1;
        isPressed = true;

    }
    IEnumerator MoveCo(Vector3 _from,Vector3 _to)
    {
        float t = 0f,speed=2f,diff=0f;
        var _transform=Vector3.zero;
        if (tempCount > 0)
        {
            diff = pillers[tempCount].transform.position.y - pillers[tempCount - 1].transform.position.y;
        }
  //      Debug.Log("diff=="+diff);
//        _to=new Vector3(_to.x,_to.y+diff,_to.z);
        Debug.Log("dist=="+_to);
        while (t<1)
        {
            t += Time.deltaTime*speed;
            _transform = Vector3.Lerp(_from, _to, t);
            camera.transform.position = _transform;
            yield return null;
        }

        camera.transform.position = _to;
    }

    public void Reset()
    {
        SceneManager.LoadScene(0);
    }
}
