﻿using System.Collections.Generic;
using _PerfectLanding._Scripts.View.Elements;
using _PerfectLanding.Scripts.View.Animation;
using Sourav.Engine.Editable.DataRelated;
using Unity.Collections;
using UnityEngine;

namespace _PerfectLanding._Scripts.Data
{
   public class CharacterData : CommonData
   {
      public bool canJump,isJumping;
      public bool isLand;
      public bool isPressed;
      public GameObject character;
      public GameObject grappingLine;
      public List<pillerHandler> pillers;
      public List<HingeJoint> joints;
      public List<HingeJoint> tempJoints;
      public int count;
      [ReadOnly]public int redAreaScore, YellowAreaScore, SideScore;
      public AnimationCurve curve;
      public CharacterAnimation characterAnimation;
   }
}
