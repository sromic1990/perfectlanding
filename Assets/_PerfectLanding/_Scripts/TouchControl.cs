﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour
{
    public Rigidbody ball; // Drag your ball here
    public Vector2 fp; // first finger position
    public Vector2 lp; // last finger position

    void Start() {
        ball = GetComponent<Rigidbody>();
    }
    
    void FixedUpdate () {
        
            if (Input.GetMouseButtonDown(0))
            {
                fp = Input.mousePosition;
                lp = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                lp = Input.mousePosition;
            }
            if(Input.GetMouseButtonUp(0))
            {
                if((fp.x - lp.x) > 80) // left swipe
                {
                    ball.AddForce(transform.forward * 150f);
                }
                else if((fp.x - lp.x) < -80) // right swipe
                {
                    ball.AddForce(transform.forward * -150f);
                    
                }
                
            }
        
    }
}
