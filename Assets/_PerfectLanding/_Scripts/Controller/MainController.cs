﻿
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using UnityEngine;

namespace _PerfectLanding._Scripts.Controller
{
    public class MainController : Sourav.Engine.Core.ControllerRelated.Controller
    {
        // Start is called before the first frame update
        void Start()
        {
            LoadGame();
        }

        private void LoadGame()
        {
//            Debug.Log("LOAD GAME");
            App.GetNotificationCenter().Notify(Notification.LoadGame);
        }
        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {
           
            switch (notification)
            {
                case Notification.GameLoaded:
                    Debug.Log("GAME LOADED");
                    if (App.GetLevelData().CurrentLevel != App.GetLevelData().NextLevel)
                    {
                        App.GetLevelData().CurrentLevel = App.GetLevelData().NextLevel;
                    }
                    if (App.GetLevelData().CurrentLevel >= App.GetLevelData().totalLevelCount)
                    {
                        App.GetLevelData().CurrentLevel = App.GetLevelData().fallBackLevel;
                    }
                    StartPlaying();
                    break;
                case Notification.LevelComplete:
                    App.GetLevelData().NextLevel = App.GetLevelData().CurrentLevel + 1;
//                    App.GetLevelData().CurrentLevel += 1;
                    App.GetLevelData().CurrentLevelActual += 1;
//                    if (App.GetLevelData().CurrentLevel >= App.GetLevelData().totalLevelCount)
//                    {
//                        App.GetLevelData().CurrentLevel = App.GetLevelData().fallBackLevel;
//                    }
                    break;
                case Notification.Replay:
                    App.GetNotificationCenter().Notify(Notification.ResetCameraPosition);
                    StartPlaying();
                    break;
                case Notification.ContinueToNextLevel:
                    Debug.Log("level"+notification);
                    App.GetLevelData().CurrentLevel = App.GetLevelData().NextLevel;
                    if (App.GetLevelData().CurrentLevel >= App.GetLevelData().totalLevelCount)
                    {
                        App.GetLevelData().CurrentLevel = App.GetLevelData().fallBackLevel;
                    }
                    App.GetNotificationCenter().Notify(Notification.ResetCameraPosition);
                    StartPlaying();
                    break;
            }
        }

        void StartPlaying()
        {
            App.GetLevelData().CurrentScore = 0;
            App.GetNotificationCenter().Notify(Notification.StartGameplay);
            App.GetNotificationCenter().Notify(Notification.UpdateScore);
        }
    }    

}

