﻿using System;
using System.Collections;
using System.Collections.Generic;
using _PerfectLanding._Scripts.Data;
using _PerfectLanding._Scripts.View.Elements;
using _PerfectLanding.Scripts.View.Animation;
using MEC;
using Sourav.Engine.Core.NotificationRelated;
using Sourav.Engine.Editable.NotificationRelated;
using Sourav.Utilities.Extensions;
using UnityEngine;

namespace _PerfectLanding._Scripts.Controller
{
    public class GameplayController : Sourav.Engine.Core.ControllerRelated.Controller
    {
        [SerializeField]private int tempCount;
        private Rigidbody characterRb;

        [SerializeField] private Transform levelData;
        private CharacterData characterCommonData;
        [SerializeField] private Transform initilCharacterTransform;
        private Vector3 initialCharacterPosition;
        // Start is called before the first frame update
        void Start()
        {
            characterRb = App.GetCharacterData().character.GetComponent<Rigidbody>();
            characterCommonData = App.GetCharacterData();
            initialCharacterPosition = initilCharacterTransform.position;
//            initialCharacterPosition = characterCommonData.character.transform.position;
            characterCommonData.grappingLine=new GameObject("GrappingLine");
            characterCommonData.grappingLine.SetActive(false);
            LineRenderer line_renderer = characterCommonData.grappingLine.AddComponent<LineRenderer>();

            line_renderer.endWidth = .1f;
            line_renderer.startWidth = .01f;
        }


        public override void OnNotificationReceived(Notification notification, NotificationParam param = null)
        {
//            Debug.Log("notification"+notification);
            switch (notification)
            {
                case Notification.StartGameplay:
                    SetHighScore();
                    InitialSetUp();
                    break;
                case Notification.Input:
                    HandleInput();
                    break;
                case Notification.JumpState:
                    characterCommonData.isPressed = true;
                    characterCommonData.canJump = false;
                    HandleJumpState();
                    break;
                case Notification.StartJump:
                    HandleJumpMove();
                    break;
                case Notification.StopJump:
                    if (!characterCommonData.isPressed) return;
                    characterCommonData.isPressed = false;
                    characterCommonData.isJumping = false;
                    characterCommonData.grappingLine.SetActive(false);
//                    characterCommonData.characterAnimation.animator.Play(CharacterAnimStates.Empty.ToString());
                    characterCommonData.characterAnimation.animator.Play(CharacterAnimStates.LandingSamePlace.ToString());
                    break;
                case Notification.Reached:
                    App.GetCharacterData().canJump = true;
                    CreateRope();
                    break;
                case Notification.SamePillerArrived:
                    UpdatePillerData(param.intData[0]);
                    break;
                case Notification.GameOver:
                    Debug.Log("Game over");
                    App.GetNotificationCenter().Notify(Notification.StopCameraScript);
                    break;
                case Notification.LevelComplete:
                    Debug.Log("Level complete");
                    SetHighScore();
                    App.GetNotificationCenter().Notify(Notification.StopCameraScript);
                    break;
            }
        }

        void UpdatePillerData(int pillerIndex)
        {
            characterCommonData.count -= 1;
            characterCommonData.tempJoints.RemoveAt(pillerIndex);
        }
        void InitialSetUp()
        {
            //
            levelData.DeleteAllChildren();
            GameObject level_g = Instantiate(Resources.Load("Prefabs/Level"+App.GetLevelData().CurrentLevel)) as GameObject;
            level_g.transform.SetParent(levelData);
            characterRb.isKinematic = true;
            Timing.CallDelayed(0.1f, () =>
            {
                
                GameObject joints=GameObject.Find("Joints");
                if (joints != null)
                {
                    characterCommonData.joints=joints.GetComponentsInChildren<HingeJoint>().ToList();
                }
                GameObject _pillers=GameObject.Find("Pillars");
                if (_pillers != null)
                {
                    characterCommonData.pillers = _pillers.GetComponentsInChildren<pillerHandler>().ToList();
                }
                characterCommonData.character.transform.position = initialCharacterPosition+new Vector3(0,1,0);
                characterRb.isKinematic = false;
                tempCount = 0;
                characterCommonData.count = 0;
//            characterCommonData.isPressed = false;
//            if(characterCommonData.tempJoints==null)
                characterCommonData.tempJoints=new List<HingeJoint>();
//            characterCommonData.tempJoints.Clear();
                for (int i = 0; i < characterCommonData.pillers.Count; i++)
                {
                    characterCommonData.pillers[i].index = i;
                }
            });
           
        }

      
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                App.GetNotificationCenter().Notify(Notification.InputDetected);
//                HandleInput();
                
            }

            if (characterCommonData.isJumping)
            {
                Vector3[] line_vertex_arr = {characterCommonData.characterAnimation.swingPivotTransform.position, characterCommonData.joints[tempCount].transform.position};
                characterCommonData.grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
                characterCommonData.grappingLine.SetActive(true);
            }
            if (Input.GetMouseButtonUp(0))
            {
                OnJumpComplete();
            }
        }

        void HandleInput()
        {
            if (!App.GetCharacterData().canJump) return;
            
           
            App.GetNotificationCenter().Notify(Notification.JumpState);
        }

        void HandleJumpState()
        {
            characterRb.constraints = RigidbodyConstraints.None;
            characterCommonData.character.GetComponent<Collider>().isTrigger = true;
            characterCommonData.grappingLine.SetActive(true);
            
            StartCoroutine(moveChar((() => 
                    //play jump anim
                    App.GetNotificationCenter().Notify(Notification.StartJump)
                    )));
            
        }

        void HandleJumpMove()
        {
            Debug.Log("handlejumpmove");
            if (!characterCommonData.isPressed) return;
//            characterCommonData.characterAnimation.animator.Play(CharacterAnimStates.JumpPlatformSame.ToString());
            HingeJoint hg = App.GetCharacterData().joints[App.GetCharacterData().count];
            App.GetCharacterData().tempJoints.Add(hg);
            
            characterCommonData.characterAnimation.OnMovementStart();
           
            var hinge = characterCommonData.joints[App.GetCharacterData().count];
            var diff = Vector3.Distance(hinge.transform.position, characterCommonData.characterAnimation.swingPivotTransform.position);
            characterCommonData.tempJoints[characterCommonData.count].connectedBody = characterRb;
            tempCount = characterCommonData.count;
            characterCommonData.count += 1;
            characterCommonData.isJumping = true;
        }
        void OnJumpComplete()
        {
            App.GetNotificationCenter().Notify(Notification.StopJump);
            for (int i = 0; i < characterCommonData.joints.Count; i++)
            {
                characterCommonData.joints[i].connectedBody = null;
            }
            characterCommonData.character.GetComponent<Collider>().isTrigger = false;
            characterCommonData.character.transform.rotation=Quaternion.Euler(0,0,0);
            characterRb.constraints = RigidbodyConstraints.FreezeRotation;
           
        }
        IEnumerator moveChar(Action OnComplete)
        {
            characterRb.isKinematic = true;
            Debug.Log("Move start");
            Vector3 pos;
            Vector3 stratPos=pos= characterCommonData.character.transform.position;
            var player = characterCommonData.character;
            Vector3 endPos=new Vector3(pos.x,pos.y+1f,pos.z-1f);
            float t = 0f;
            while (endPos!=player.transform.position)
            {
                t += Time.deltaTime;
                player.transform.position = Vector3.Lerp(stratPos, endPos, characterCommonData.curve.Evaluate(t));
                Vector3[] line_vertex_arr = {characterCommonData.characterAnimation.swingPivotTransform.position, characterCommonData.joints[characterCommonData.count].transform.position};
                characterCommonData.grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
                yield return null;
            }
            characterRb.isKinematic = false;
            yield return new WaitForEndOfFrame();
            Debug.Log("Move complete"+characterRb.isKinematic);
            if (OnComplete != null) OnComplete();

        }
        private void CreateRope()
        {
            if (characterCommonData.joints.Count == characterCommonData.count)
            {
                App.GetNotificationCenter().Notify(Notification.LevelComplete);
                characterCommonData.characterAnimation.animator.Play(CharacterAnimStates.Empty.ToString());
                return;
            }
            characterCommonData.characterAnimation.animator.Play(CharacterAnimStates.Swing.ToString());
            Vector3[] line_vertex_arr = {characterCommonData.characterAnimation.swingPivotTransform.position, characterCommonData.joints[characterCommonData.count].transform.position};
            Debug.Log("rope"+Vector3.Distance(characterCommonData.joints[characterCommonData.count].transform.position,characterCommonData.character.transform.GetChild(0).position));
            Debug.Log("len"+(characterCommonData.joints[characterCommonData.count].transform.position.y-characterCommonData.character.transform.GetChild(0).localPosition.y));
            characterCommonData.grappingLine.GetComponent<LineRenderer>().SetPositions(line_vertex_arr);
            characterCommonData.grappingLine.SetActive(true);
        
        }

        void SetHighScore()
        {
            int highScore = App.GetLevelData().HighScore;
            if (highScore < App.GetLevelData().CurrentScore)
            {
                highScore = App.GetLevelData().CurrentScore;
            }

            App.GetLevelData().HighScore = highScore;
            App.GetNotificationCenter().Notify(Notification.SetHighestScore);
        }
    }
}
