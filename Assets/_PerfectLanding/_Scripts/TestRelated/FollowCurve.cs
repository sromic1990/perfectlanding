﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class FollowCurve : MonoBehaviour
{

    public AnimationCurve xCurve, yCurve;
    public Rigidbody body;

    private int curvePosition = 0;

    private float timeElapsed = 0;

    private bool started = false;

    private Vector2 startPosition;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!started)
        {
            started = true;
            timeElapsed = 0;
            startPosition = transform.position;
        }
        else
        {
            timeElapsed = Time.deltaTime;
            body.MovePosition(new Vector3(startPosition.x+xCurve.Evaluate(timeElapsed),
                startPosition.y+yCurve.Evaluate(timeElapsed),0));
        }
    }
}
