﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSystem : MonoBehaviour
{
    public GameObject hooked_node;
    public GameObject roperHingeAnchor;
    public HingeJoint ropeJoint;
    public ConfigurableJoint configJoint;
    public bool ropeAttached;
    private Vector2 playerPosition;
    private Rigidbody ropeHingeAnchorRb;
    // Start is called before the first
    //
    // frame update
    private void Awake()
    {
        
        ropeHingeAnchorRb = roperHingeAnchor.GetComponent<Rigidbody>();
        
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var facingDirection = hooked_node.transform.position - transform.position;
        var aimAngle = Mathf.Atan2(facingDirection.z, facingDirection.x);
        if (aimAngle < 0f)
        {
            aimAngle = Mathf.PI * 2 + aimAngle;
        }

        var aimDirection = Quaternion.Euler(0, 0, aimAngle * Mathf.Rad2Deg);

        playerPosition = transform.position;
    }
    public LineRenderer ropeRenderer;
    public LayerMask ropeLayerMask;
    private float ropeMaxCastDistance = 20f;
    private List<Vector3> ropePositions = new List<Vector3>();
    // 1
    private void HandleInput(Vector2 aimDirection)
    {
        if (Input.GetMouseButton(0))
        {
            // 2
            if (ropeAttached) return;
           // ropeRenderer.enabled = true;

            var hit = Physics2D.Raycast(playerPosition, aimDirection, ropeMaxCastDistance, ropeLayerMask);
        
            // 3
           
                ropeAttached = true;
               // if (!ropePositions.Contains(hit.point))
               // {
                    // 4
                    // Jump slightly to distance the player a little from the ground after grappling to something.
                    transform.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 2f,2f), ForceMode.Impulse);
                    ropePositions.Add(hooked_node.transform.position);
                   // ropeJoint.
                   // ropeJoint.distance = Vector2.Distance(playerPosition, hit.point);
                   // ropeJoint.enabled = true;
                  //  ropeHingeAnchorSprite.enabled = true;
               // }
                 //   ropeJoint.velocity
        }

        if (Input.GetMouseButton(1))
        {
            ResetRope();
        }
    }

// 6
    private void ResetRope()
    {
      //  ropeJoint.enabled = false;
        ropeAttached = false;
      //  playerMovement.isSwinging = false;
        ropeRenderer.positionCount = 2;
        ropeRenderer.SetPosition(0, transform.position);
        ropeRenderer.SetPosition(1, transform.position);
        ropePositions.Clear();
       // ropeHingeAnchorSprite.enabled = false;
    }

}
