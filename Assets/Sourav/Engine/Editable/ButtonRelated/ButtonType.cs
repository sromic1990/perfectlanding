﻿using UnityEngine;

namespace Sourav.Engine.Editable.ButtonRelated
{
    //FIXME Fill this up with button types
    public enum ButtonType : int
    {
        Play,
        Pause,
        Settings,
        RestorePurchase = 5,
        
        PopUpClose = 14,
        Store = 15,
        Home = 16,
        
        Coin1,
        Coin2,
        Coin3,
        Coin4,
        Coin5,
        Coin6,
        RemoveAds,
        WatchVideoAd,
        
        HelloWorld,
        Replay,
        NextLevel,
    }
}
