﻿using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using Sourav.Engine.Core.GameElementRelated;
using Sourav.Engine.Editable.NotificationRelated;
using Sourav.Engine.Editable.PopUpRelated.Model;
using Sourav.Utilities.Extensions;
using UnityEngine;

namespace Sourav.Engine.Editable.PopUpRelated.Element
{
    public class PopUpView : GameElement
    {
        #region FIELDS
        [SerializeField] private GameObject popUpPanel;
        [SerializeField] private List<PopUpHolder> popUpHolders;
        [ReadOnly][SerializeField] private GameObject currentPopUp;
        [ReadOnly] [SerializeField] private bool isPopUpOpen;

        [SerializeField] private GameObject[] hideWhilePopUpShow;
        #endregion

        #region METHODS
        #region PUBLIC METHODS
        [Sirenix.OdinInspector.Button()]
        public void HidePopUp()
        {
            if (currentPopUp == null)
            {
                HideAllPopUps();
            }
            else
            {
                DOTween.Restart(currentPopUp, "out");
                currentPopUp = null;
            }
        }

        [Sirenix.OdinInspector.Button()]
        public void HidePopUpSilently()
        {
            if (currentPopUp == null)
            {
                HideAllPopUps();
            }
            else
            {
                currentPopUp.transform.localScale = Vector3.zero;
                currentPopUp = null;
                OnHidePopUpComplete();
            }
        }

        [Sirenix.OdinInspector.Button()]
        public void ShowPopUp(PopUpType type)
        {
           
            GameObject popUp = GetCorrectPopUp(type);
            if (popUp != null)
            {
                Debug.Log(popUp+"show"+type);
                popUpPanel.Show();
                isPopUpOpen = true;
                currentPopUp = popUp;
                currentPopUp.transform.localScale = Vector3.zero;
                currentPopUp.Show();
                DOTween.Restart(currentPopUp, "in");           
                App.GetLevelData().isPopUpShown = true;
            }
        }

        public bool IsPopUpOen()
        {
            return isPopUpOpen;
        }
        #endregion

        #region PRIVATE METHODS
        private void HideAllPopUps()
        {
            for (int i = 0; i < popUpHolders.Count; i++)
            {
                popUpHolders[i].popUpObject.transform.localScale = Vector3.zero;
                popUpHolders[i].popUpObject.Hide();
            }
            OnHidePopUpComplete();
        }
        #endregion

        #region EVENT METHODS
        public void OnHidePopUpComplete()
        {
            isPopUpOpen = false;
            popUpPanel.Hide();

            App.GetLevelData().isPopUpShown = false;
            
//            ShowAllHiddenObjects();
        }
        public void OnPopUpShowComplete()
        {
            isPopUpOpen = true;
            App.GetNotificationCenter().Notify(Notification.PopUpShown);
        }
        
        public void OnPopUpOpen()
        {
            App.GetNotificationCenter().Notify(Notification.PopUpOpen);
        }

        private void ShowAllHiddenObjects()
        {
            for (int i = 0; i < hideWhilePopUpShow.Length; i++)
            {
                hideWhilePopUpShow[i].Show();
            }
        }

        private void HideAllHiddenObjects()
        {
            for (int i = 0; i < hideWhilePopUpShow.Length; i++)
            {
                hideWhilePopUpShow[i].Hide();
            }
        }
       
        #endregion

        #region HELPER METHODS
        private GameObject GetCorrectPopUp(PopUpType type)
        {
            for (int i = 0; i < popUpHolders.Count; i++)
            {
                if (type == popUpHolders[i].type)
                {
                    return popUpHolders[i].popUpObject;
                }
            }

            return null;
        }
        #endregion
        #endregion
    }

    [System.Serializable]
    public class PopUpHolder
    {
        public PopUpType type;
        public GameObject popUpObject;
    }
}
