﻿namespace Sourav.Engine.Editable.PopUpRelated.Model
{
    public enum PopUpType
    {
        Store,
        Settings,
        NewItemUnlocked,
        GameOver,
        LevelComplete,
        Ingredients,
        RateUs,
    }
}
