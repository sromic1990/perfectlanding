﻿namespace Sourav.Engine.Editable.ControllerRelated
{
	//FIXME Fill this up with game controllers
	public enum ControllerType
	{
		None,
		GameController,
		UiController,
		UiScreenController,
		PauseResumeController,
		ButtonController,
		CameraController,
		SaveLoadController,
		PositionRegistryController,
		AudioController,
		TutorialController,
		PopUpController,
		IntroController,
		
		//Toggles Related
		ToggleHandler,
		
		//Plugins controller
		AdMobController,
		FacebookController,
		PurchaseController,
		HapticFeedbackController,
		AppsFlyerController,
		
		//Gameplay Related
		GameplayController,
		BallController,
		HookController,
		
		//Spawner Related
		HookSpawnerController,
		LevelConstructor,
		
	}
}
