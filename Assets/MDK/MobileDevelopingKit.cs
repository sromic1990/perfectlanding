﻿using UnityEngine;
using System.Runtime.InteropServices;

public class MobileDevelopingKit : MonoBehaviour
{

    public float DesignWidth; //The width which you design your game/ui around.
    public float DesignHeight; //The height which you design your game/ui around.

    public string AppStoreID; //write your App ID. Example: 123456789
    public string PlayStoreID; //write your App ID. Example: com.companyName.gameName

    void Start()
    {
        Camera.main.aspect = DesignWidth / DesignHeight;       
    }

    public void RateUs()
    {

#if UNITY_ANDROID //if the device is Android
        Application.OpenURL("http://play.google.com/store/apps/details?id="+ PlayStoreID); //Opens your app page in Google Play Store
#elif UNITY_IOS //else if the device is iOS
        Application.OpenURL("https://itunes.apple.com/app/id"+ AppStoreID); //Opens your app page in Apple App Store
#else
		Debug.Log("No RateUs set up for this platform."); //Consider openning your website/twitter.
#endif

    }

    public void Share() //Call this method to show the share options.
    {
        string TitleOfMessage = "This is Title"; //Change this to your desired title.
        string MessageText = "I got Score 123 in game xyz"; //Change this to your desired body.

        ShareConfig(MessageText, TitleOfMessage);//Calls the method responsible for sharing in iOS and Android.
    }

    public void ShareConfig(string TextToShare, string SubjectOfTheMessage)
    {
#if UNITY_ANDROID //if the device is Android
        AndroidJavaClass AJClass = new AndroidJavaClass("android.content.Intent"); //AndroidJavaClass is the Unity representation of a generic instance of java.lang.Class.
        AndroidJavaObject AJObject = new AndroidJavaObject("android.content.Intent"); //AndroidJavaObject is the Unity representation of a generic instance of java.lang.Object. 
        AJObject.Call<AndroidJavaObject>("setAction", AJClass.GetStatic<string>("ACTION_SEND"));//Calls the java method setAction
        AJObject.Call<AndroidJavaObject>("setType", "text/plain");//Calls the java method setType
        AJObject.Call<AndroidJavaObject>("putExtra", AJClass.GetStatic<string>("EXTRA_SUBJECT"), SubjectOfTheMessage); //Calls the java method putExtra
        AJObject.Call<AndroidJavaObject>("putExtra", AJClass.GetStatic<string>("EXTRA_TEXT"), TextToShare); //Calls the java method putExtra
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject jChooser = AJClass.CallStatic<AndroidJavaObject>("createChooser", AJObject, SubjectOfTheMessage);     
        currentActivity.Call("startActivity", jChooser);//Calls the java method startActivity

#elif UNITY_IOS //else if the device is iOS
        CalliOSShare(TextToShare, SubjectOfTheMessage);

#else //if the device is not iOS nor Android
		Debug.Log("The Platform Is Not iOS not Android.");

#endif
    }

#if UNITY_IOS //if the device is iOS
    public struct ConfigStruct
    {
        public string title;
        public string message;
    }

    public struct SocialSharingStruct
    {
        public string text;
        public string url;
        public string image;
        public string subject;
    }

    [DllImport("__Internal")]
    private static extern void showSocialSharing(ref SocialSharingStruct conf);

    public static void CalliOSShare(string txt, string subject)
    {
        SocialSharingStruct conf = new SocialSharingStruct();
        conf.text = txt;
        conf.subject = subject;
        conf.url = "";
        conf.image = "";

        showSocialSharing(ref conf);
    }
#endif
}
