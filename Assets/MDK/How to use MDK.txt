Hello, Thanks for purchasing MDK. 
If you have any questions, do not hesitate to email me at Shnayzr@gmail.com

MDK is very simple and easy. no scripting or coding is needed


++Follow these steps and everything will be good:

1-Create an empty GameObject.

2-Attach the MobileDevelopingKit script to the GameObject you just created.

3-Now create a button which calls either RateUs() or Share() Methods.

4-Done.


++Configurations:

Share: to change the text you want the player to share simply go to the script MobileDevelopingKit
and change MessageText value to the string you want.

Rate Us: Change AppStoreID and PlayStoreID to your app id (You can find these in your developer account)

Multi-Resolution: Change DesignWidth and DesignHeight to your design numbers. The change to resolution
and calculations will happen when the game starts. (change resolutions from the drop down menu to check it)


++How to Run the demo scene.

To check Multi-Resolution, just change the resolution of the game from the drop down menu.

To check share and rate us buttons you will need build the game on an actual android or iOS device.
